<?php

namespace App\Service;


class CalculatorService
{
    public static function addition(int $a, int $b)
    {
        $result = $a + $b;

        return $result;
    }

    public static function substraction(int $a, int $b)
    {
        $result = $a - $b;

        return $result;
    }

    public static function multiplication(int $a, int $b)
    {
        $result = $a * $b;

        return $result;
    }

    public static function division(int $a, int $b)
    {
        try {
            $result = $a / $b;
            return $result;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

    
}