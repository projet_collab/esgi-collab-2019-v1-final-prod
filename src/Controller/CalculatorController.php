<?php

namespace App\Controller;

use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/calculator/addition/{numbers}/", name="addition")
     */
    public function addition(CalculatorService $calculatorservice, Request $request)
    {
        $numbers = $request->get('numbers');
        $numbers = \preg_split("/\,/", $numbers);

        $result = $calculatorservice->addition($numbers[0],$numbers[1]);

        return $this->json(['result' => $result]);
    }
    /**
     * @Route("/calculator/substraction/{numbers}/", name="substraction")
     */
    public function substraction(CalculatorService $calculatorservice, Request $request)
    {
        $numbers = $request->get('numbers');
        $numbers = \preg_split("/\,/", $numbers);

        $result = $calculatorservice->substraction($numbers[0],$numbers[1]);

        return $this->json(['result' => $result]);
    }
    /**
     * @Route("/calculator/multiplication/{numbers}/", name="multiplication")
     */
    public function multiplication(CalculatorService $calculatorservice, Request $request)
    {
        $numbers = $request->get('numbers');
        $numbers = \preg_split("/\,/", $numbers);

        $result = $calculatorservice->multiplication($numbers[0],$numbers[1]);

        return $this->json(['result' => $result]);
    }
    /**
     * @Route("/calculator/division/{numbers}/", name="division")
     */
    public function division(CalculatorService $calculatorservice, Request $request)
    {
        $numbers = $request->get('numbers');
        $numbers = \preg_split("/\,/", $numbers);

        $addition = $calculatorservice->division($numbers[0],$numbers[1]);

        return $this->json(['result' => $addition]);
    }

   
}
