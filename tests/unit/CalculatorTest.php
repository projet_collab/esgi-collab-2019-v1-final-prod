<?php
//tests/unit/CalculatorTest.php
namespace App\tests\unit;

use App\Service\CalculatorService;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testAddition()
    {
        $calculator = new CalculatorService();
        $result = $calculator->addition(1, 2);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(3, $result);
    }
    public function testSubstraction()
    {
        $calculator = new CalculatorService();
        $result = $calculator->substraction(4, 2);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(2, $result);
    }
    public function testMultiplication()
    {
        $calculator = new CalculatorService();
        $result = $calculator->multiplication(4, 2);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(8, $result);
    }
    public function testDivision()
    {
        $calculator = new CalculatorService();
        $result = $calculator->division(4, 2);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(2, $result);
    }

}