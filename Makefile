install:
	composer install
	composer update

test:
	php bin/phpunit

start docker:
	docker-compose up -d

sh:
	docker exec -it php-esgi-collab /bin/bash
