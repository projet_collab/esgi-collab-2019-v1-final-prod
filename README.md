# Docker Symfony (PHP7-FPM - NGINX )

[![Build Status](https://travis-ci.org/maxpou/docker-symfony.svg?branch=master)](https://travis-ci.org/maxpou/docker-symfony)


Docker-symfony gives you everything you need for developing Symfony application. This complete stack run with docker and [docker-compose (1.7 or higher)](https://docs.docker.com/compose/).

## Installation

1. clone the project according to your symfony application in your repository 


2. Build (if needed) and start the two docker containers, nginx and php.
    ```bash
    $ make start docker
    ```

3. You can then use make sh to connect to the php container and run :
    ```bash
    $ make install
    ```
## Usage

* Symfony app: visit [localhost:8000/calculator](http://localhost:8000/)
* API : the api endpoint are:
    * /calculator/addition/{numbers}
    * /calculator/substraction/{numbers}
    * /calculator/multiplication/{numbers}
    * /calculator/division/{numbers}

Where {numbers} is two integers separated by a coma (","), like: /calculator/addition/1,3

## Tests
You first start off by installing Composer using the following command
    ```
    make install
    ```

You will then be able to test if the app works well using the following command
    ```
    make test
    ```
## Customize

If you want to add optionnals containers like Redis, PHPMyAdmin... take a look on [doc/custom.md](doc/custom.md).

## How it works?

Have a look at the `docker-compose.yml` file, here are the `docker-compose` built images:


* `php`: This is the PHP-FPM container in which the application volume is mounted,
* `nginx`: This is the Nginx webserver container in which application volume is mounted too,

This results in the following running containers:

```bash
$ docker-compose ps
                       Name                                      Command              State          Ports        
------------------------------------------------------------------------------------------------------------------
esgi-collab-2019-v1-final-prod_nginx_1_b79c8da37e04   nginx -g daemon off;            Up      0.0.0.0:8000->80/tcp
esgi-collab-2019-v1-final-prod_php_1_351310b79e14     docker-php-entrypoint php-fpm   Up      9000/tcp 

## Useful commands

```bash
# bash commands
$ docker-compose exec php bash

# Composer (e.g. composer update)
$ docker-compose exec php composer update

# Same command by using alias
$ docker-compose exec php bash
$ sf cache:clear


# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```

## FAQ

* Got this error: `ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?
If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.` ?  
Run `docker-compose up -d` instead.

* Permission problem? See [this doc (Setting up Permission)](http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup)

## Security

If you discover a security vulnerability within the app, please send an e-mail to groupe2@esgi.fr.

## Contributing

**thank you** for contributing ♥  

